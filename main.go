package main

import (
	"codeberg.org/dralmaember/vatten/ui"
	"github.com/rivo/tview"
)

func main() {
	app := tview.NewApplication()
	root := ui.Construct(app)
	if err := app.SetRoot(root, true).Run(); err != nil {
		panic(err)
	}
}
