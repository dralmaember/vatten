package ui

import (
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

var (
	app *tview.Application
)

func Construct(appArg *tview.Application) *tview.Grid {
	app = appArg

	app.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Key() == tcell.KeyCtrlQ {
			app.Stop()
		}

		return event
	})

	grid := tview.NewGrid().
		SetRows(2, 0, 3).
		SetColumns(20, 0, 0)
	grid.SetBorder(true).
		SetBackgroundColor(tcell.ColorBlack)

	grid.AddItem(header(), 0, 0, 1, 3, 0, 0, false)
	grid.AddItem(footer(), 2, 0, 1, 3, 0, 0, false)

	return grid
}

func header() tview.Primitive {
	return tview.NewTextView().
		SetTextAlign(tview.AlignCenter).
		SetText("Vatten - SPDP client")
}

func footer() tview.Primitive {
	return tview.NewTextView().
		SetTextAlign(tview.AlignLeft).
		SetText("\n\tC-q: quit")
}
